## Prerequisite：签署 Individal CLA

## STEP1： clone至本地环境并新增上游分支



![image-20220428174159527](/Users/hermione_k/Library/Application Support/typora-user-images/image-20220428174159527.png)

![image-20220428174229874](/Users/hermione_k/Library/Application Support/typora-user-images/image-20220428174229874.png)

“当我们在使用git clone的时候，git会自动地**将这个远程的****repo****命名为****origin**，拉取它所有的数据之后，创建一个指向它master的指针，命名为origin/master，之后会在本地创建一个指向同样位置的指针，命名为master，和远程的master作为区分。”

有本地仓库分支mater，远程分支origin/master和upstream，upstream远端分支是fork的官方仓库，以便同步官方仓库，解决pr冲突。

## STEP2： 创建本地开发分支

![image-20220428175839219](/Users/hermione_k/Library/Application Support/typora-user-images/image-20220428175839219.png)

## STEP3: 获取上游分支最新版本并与当前分支合并

![image-20220428180710075](/Users/hermione_k/Library/Application Support/typora-user-images/image-20220428180710075.png)

最好按照commit，git pull -rebase，push的顺序操作以防止所做的更改消失。

reference：https://blog.csdn.net/PlayGrrrrr/article/details/114979366

## STEP4: 新建文件夹

![image-20220428181932006](/Users/hermione_k/Library/Application Support/typora-user-images/image-20220428181932006.png)

## STEP5: 将更新的内容添加到暂缓区，提交到本地仓库并push到自己的远程仓库

## STEP6: 到gittee上提交PR

正规做法：先交issue再提交PR